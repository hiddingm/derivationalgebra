(* ::Package:: *)

BeginPackage["DerivationAlgebra`"];

ExpandNCM::usage = "";
ExpandCM::usage = "";
CMEv::usage = "";
opAct::usage = "";
CMAct::usage = "";
CM::usage = "";
\[Epsilon]::usage = "";
z::usage = "";

Begin["`Private`"];

Needs["DerivationAlgebra`Helpers`"];
Needs["DerivationAlgebra`HallBasis`"];

(* Expanding NonCommutativeMultiply *)
ExpandNCM[(h : NonCommutativeMultiply)[]] := 1;
ExpandNCM[(h : NonCommutativeMultiply)[a___, 0, c___]] := 0;

ExpandNCM[(h : NonCommutativeMultiply)[a___, SubPlus[Global`\[Beta]][args__](b_:1), c___]] := SubPlus[Global`\[Beta]][args] ExpandNCM[a ** b ** c];
ExpandNCM[(h : NonCommutativeMultiply)[a___, SubPlus[Global`\[Beta]][args__](b_:1), c___]] := SubMinus[Global`\[Beta]][args] ExpandNCM[a ** b ** c];
ExpandNCM[(h : NonCommutativeMultiply)[a___, 1, c___]] := ExpandNCM[a ** c];

ExpandNCM[(h : NonCommutativeMultiply)[a___, b_Plus, c___]] := Distribute[h[a, b, c], Plus, h, Plus, ExpandNCM[h[##]] &];
ExpandNCM[(h : NonCommutativeMultiply)[a___, b_Times, c___]] := Most[b]ExpandNCM[h[a, Last[b], c]];
ExpandNCM[a_] := ExpandAll[a];

(* Expanding CM linearly (preferrably not used often since it is slow.)*)
ExpandCM[ex_, scalarsForm_List : {}] := ex //. {
	CM[a___, b_, c___] /; b[[0]] === Plus :> (CM[a, #, c]& /@ b),
	CM[a___, b_ d_, c___] /; (NumericQ[b] || MatchQ[b, Alternatives @@ scalarsForm]) :> b CM[a, d, c],
	CM[a___, b_ d_, c___] /; (NumericQ[b] || MatchQ[b, Alternatives @@ scalarsForm^_]) :> b CM[a, d, c],
	CM[a___, 0, b___] :> 0
};

CMEv[a_, b_] := CMEv[a, b] = (a**b - b**a /. CM[l_, r_] :> CMEv[l, r] /. NonCommutativeMultiply[args___] :> ExpandNCM[NonCommutativeMultiply[args]]);

ClearAll[applyfDer];
applyfDer[f_, ex_, base_ : {}] := (*applyfDer[f,ex,base]=*)Which[
  (* If in A just evaluate. *)
  (ex[[0]] === Symbol) || (ex[[0]] === Integer),
  MapOverCM[MakeHall, f[ex] /. base],
  
  (ex[[0]] === Plus) || (MatchQ[a_CM[__], ex[[0]]]) || (MatchQ[-CM[__], ex[[0]]]),
  (* If a sum, map over it *)
  MapOverCM[applyfDer[f, #, base] &, ex],
  
  !IsHall[ex],
  (* Not Hall also map over it. *)
  MapOverCM[applyfDer[f, #, base] &, ex],
  
  ex[[0]] === CM,
  (* Now we have a commutator, and we may assume it is of the form [h1, h2] with h1 and h2 Hall *)
  With[{fa = applyfDer[f, ex[[1]], base], fb = applyfDer[f, ex[[2]], base]},
    (* Use rule for derivations: *)
    MapOverCM[MakeHall, MapOverCM[CM[#, ex[[2]]] &, fa]] + MapOverCM[MakeHall, MapOverCM[CM[ex[[1]], #] &, fb]] /. CM[___, 0, ___] :> 0
  ]
];

opAct[head_, x_, base_] := MapOverCM[applyfDer[head, #, base]&, x // Expand] // Expand;
CMAct[xx_, yy_, base_][a___] := CMAct[xx,yy, base][a] = Expand[opAct[xx, Expand[opAct[yy,a]], base] - opAct[yy, Expand[opAct[xx,a]], base]] // MakeHallEx;

End[];

EndPackage[];
