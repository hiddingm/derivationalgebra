(* ::Package:: *)

BeginPackage["DerivationAlgebra`Helpers`"];

SplitSum::usage = "";
MapOverCM::usage = "";
SplitSumCM::usage = "";
Convert\[Epsilon]Notation::usage = "";

Begin["`Private`"];
Needs["DerivationAlgebra`"];

SplitSum[ex_] := If[ex[[0]] === Plus, List @@ ex, {ex}];

SmartCMSplit[ex_] := With[{tmp = ex /. {_CM -> 1, a -> 1, b -> 1}}, 
  {tmp, ex/tmp}
];

SmartCMSplit[ex_, f_] := With[{tmp = ex /. {_CM -> 1, a -> 1, b -> 1}}, 
  {tmp, f[ex/tmp]}
];

SplitSumCM[0] := {{}, {}};
SplitSumCM[ex_] := Transpose[SmartCMSplit /@ SplitSum[Expand[ex]]];

MapOverCM[f_, ex_] := With[{tmp = SplitSumCM[ex]}, 
  tmp[[1]] (f /@ tmp[[2]]) // Total // Expand
];

Convert\[Epsilon]Notation[ex_]:=ex//.{
  CM[Subscript[\[Epsilon], k_],Subscript[\[Epsilon], 0]]:>-Subscript[\[Epsilon], k][1],
  CM[Subscript[\[Epsilon], k_][j_],Subscript[\[Epsilon], 0]]:>-Subscript[\[Epsilon], k][j+1],
  CM[a___,-b_,c___]:>-CM[a,b,c]
};

End[];

EndPackage[];
