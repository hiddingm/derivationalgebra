(* ::Package:: *)

BeginPackage["DerivationAlgebra`HallBasis`"];

HallOrder::usage = "";
IsHall::usage = "";
GetHallBasis::usage = "";
GetHallBasis\[Epsilon]kj::usage = "";
MakeHall::usage = "";
MakeHallEx::usage = "";

Begin["`Private`"];
Needs["DerivationAlgebra`"];
Needs["DerivationAlgebra`Helpers`"];

HallOrder[a_,b_] := LexicographicOrder[{a} /. CM -> List // Flatten, {b} /. CM -> List // Flatten];
HallSmaller[a_,b_] := HallOrder[a, b] == 1 && !(a === b);

IsHall[a_Symbol] := True;
IsHall[a_Integer] := True;
IsHall[Subscript[\[Epsilon], _]] := True;
IsHall[Subscript[z, _]] := True;
IsHall[Subscript[\[Epsilon], _][_]] := True;

IsHall[CM[a_, b_]] := IsHall[CM[a, b]] = (Which[
  !(IsHall[a] && IsHall[b]),
  False,

  !HallSmaller[a, b],
  False,

  !a[[0]] === CM,
  True,

  True,
  IsHall[a[[1]]] && IsHall[a[[2]]] && !HallSmaller[a[[2]], b]
]);

GetHallBasisInt[n_, 0] = {};
GetHallBasisInt[n_, 1] := Range[n];
GetHallBasisInt[n_, m_] := GetHallBasisInt[n, m] = Module[{},
  Table[Flatten[Outer[CM, GetHallBasisInt[n, d], GetHallBasisInt[n, m - d]]] // Union // Select[#, IsHall] &, {d, 1, m - 1}] // Flatten // Union
];

MakeHall[a_Symbol] := a;
MakeHall[a_Integer] := a;
MakeHall[CM[a_, b_]]:= MakeHall[CM[a, b]] = Which[
  a === b,
  (* If they are equal just give 0*)
  0,

  IsHall[CM[a, b]],
  (* Base case: just return the commutator as it is. *)
  CM[a, b],

  !IsHall[a],
  (* Recurse *)
  MapOverCM[MakeHall[CM[#, b]] &, MakeHall[a]],

  !IsHall[b],
  (* Recurse *)
  MapOverCM[MakeHall[CM[a, #]] &, MakeHall[b]],

  True,
  (* a and b are both Hall, but [a, b] is not. Implement the rules that Bram defined *)
  Which[
    !HallSmaller[a, b],
    (* Flip and try again *)
    -MakeHall[CM[b, a]],

    True,
    (* Perform a transformation and recurse *)
    MakeHall[CM[CM[First[a], b], Last[a]]] + MakeHall[CM[First[a], CM[Last[a], b]]]
  ]
];

MakeHallEx[ex_] := MapOverCM[MakeHall, ex // Expand] // Expand;

GetHallBasis[els_List, depth_] := Module[{elsunion = Union[els]},
	GetHallBasisInt[Length @ elsunion, depth] /. i_Integer :> elsunion[[i]]
];

GetHallBasis\[Epsilon]kj[weight_, depth_] := Module[{Ks, els, f},
	(* Computes the Hall basis for the Subscript[\[Epsilon], k][j], while filtering out unnecessary terms at intermediate steps. *)
	Ks = IntegerPartitions[weight / 2] * 2 // Select[#, !MemberQ[#, 2]&]&;
	Ks = Ks // SortBy[{Total[#], Length[#]}&];
	Ks = Select[Ks, Length[#] <= depth &];
	
	Table[
		els = K // Map[Table[{#, j}, {j, 0, # - 2}]&] // Tuples // Map[Sort] // Union;
		els = Select[els, Total[#[[All, 2]]] + Length[#] == depth &];
		els = MapAt[Subscript[\[Epsilon], #[[1]]][#[[2]]]&, els, {All, All}];
		
		els // Map[GetHallBasis[#, Length[#]]&] // Map[Select[#, And[
			(Total[(Flatten[# /. CM -> List] /. Subscript[\[Epsilon], k_][j_] :> k)] == weight),
			(Total[(Flatten[# /. CM -> List] /. Subscript[\[Epsilon], k_][j_] :> j + 1)] == depth)
			]&]&]
	, {K, Ks}] // Flatten // Union // ReplaceAll[Subscript[\[Epsilon], k_][0] :> Subscript[\[Epsilon], k]]
];

End[];

EndPackage[];
